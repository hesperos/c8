#include "c8_platform_null.h"

#include <stdio.h>
#include <stdlib.h>

static void _platformNullOpen(struct C8Platform* platform)
{
    (void)platform;
    printf("%s\n", __FUNCTION__);
}

static void _platformNullClose(struct C8Platform* platform)
{
    (void)platform;
    printf("%s\n", __FUNCTION__);
}

static int8_t _platformNullGetInput(struct C8Platform* platform)
{
    (void)platform;
    printf("%s\n", __FUNCTION__);
    return C8_INPUT_NONE;
}

static void _platformNullClearDisplay(struct C8Platform* platform)
{
    (void)platform;
    printf("%s\n", __FUNCTION__);
}

static uint8_t _platformNullDisplaySprite(
        struct C8Platform* platform,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n)
{
    (void)platform;
    printf("%s: x: %d, y: %d, n: %d\n",
            __FUNCTION__,
            x, y, n);
    return 0x00;
}

static void _platformNullStep(struct C8Platform* platform)
{
    platform->input = C8_INPUT_NONE;
}

static void _platformNullNoBeep(struct C8Platform* platform, uint8_t beepOn)
{
    (void)platform;
    beepOn = 0;
    printf("%s\n", __FUNCTION__);
}

uint8_t c8PlatformImplInitialize(struct C8Platform* platform)
{
    platform->impl = NULL;
    platform->platformOpen = _platformNullOpen;
    platform->platformClose = _platformNullClose;
    platform->getInput = _platformNullGetInput;
    platform->clearDisplay = _platformNullClearDisplay;
    platform->displaySprite = _platformNullDisplaySprite;
    platform->platformStep = _platformNullStep;
    platform->beepControl = _platformNullNoBeep;
    printf("%s\n", __FUNCTION__);
    return 0;
}

void c8PlatformImplDeInitialize(struct C8Platform* platform)
{
    printf("%s\n", __FUNCTION__);
}

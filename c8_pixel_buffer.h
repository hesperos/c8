#ifndef C8_PIXEL_BUFFER_H_
#define C8_PIXEL_BUFFER_H_

#include <stdint.h>
#include <stdlib.h>

struct C8PixelBuffer
{
    uint8_t *pixels;
    uint16_t size;
    uint8_t lineWidth;
    uint8_t halfMode;
};

void c8PixelBufferInitialise(struct C8PixelBuffer* pxb,
        uint8_t width,
        uint8_t height);

void c8PixelBufferSetHalfMode(struct C8PixelBuffer* pxb,
        uint8_t isEnabled);

void c8PixelBufferDeinitialise(struct C8PixelBuffer* pxb);

uint8_t c8PixelBufferGet(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y);

void c8PixelBufferSet(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y);

void c8PixelBufferClear(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y);

void c8PixelBufferClearAll(struct C8PixelBuffer* pxb);

void c8PixelBufferSetValue(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y,
        uint8_t value);

void c8PixelBufferScrollVert(struct C8PixelBuffer* pxb,
        uint8_t n);

void c8PixelBufferScrollHoriz(struct C8PixelBuffer* pxb,
        uint8_t direction);

#endif /* C8_PIXEL_BUFFER_H_ */

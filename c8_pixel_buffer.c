#include "c8_pixel_buffer.h"
#include "c8_platform.h"

#include <string.h>

static void _getBitAddress(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y,
        uint16_t* byteIdx,
        uint8_t* bitIdx)
{
    const uint16_t idx = x + y * (pxb->lineWidth >> pxb->halfMode);
    *byteIdx = (idx >> 3) % pxb->size;
    *bitIdx = idx & 0x07;
}

void c8PixelBufferInitialise(struct C8PixelBuffer* pxb,
        uint8_t width,
        uint8_t height)
{
    pxb->lineWidth = width;
    pxb->size = (width * height) >> 3;
    pxb->pixels = malloc(pxb->size);
    pxb->halfMode = 0;
    c8PixelBufferClearAll(pxb);
}

void c8PixelBufferSetHalfMode(struct C8PixelBuffer* pxb,
        uint8_t isEnabled)
{
    pxb->halfMode = isEnabled & 0x01;
}

uint8_t c8PixelBufferGet(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y)
{
    uint16_t byteIdx = 0;
    uint8_t bitIdx = 0;
    _getBitAddress(pxb, x, y, &byteIdx, &bitIdx);
    return (pxb->pixels[byteIdx] >> bitIdx) & 0x01;
}


void c8PixelBufferSet(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y)
{
    uint16_t byteIdx = 0;
    uint8_t bitIdx = 0;
    _getBitAddress(pxb, x, y, &byteIdx, &bitIdx);
    pxb->pixels[byteIdx] |= 0x01 << bitIdx;
}


void c8PixelBufferClearAll(struct C8PixelBuffer* pxb)
{
    memset(pxb->pixels, 0x00, pxb->size);
}


void c8PixelBufferClear(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y)
{
    uint16_t byteIdx = 0;
    uint8_t bitIdx = 0;
    _getBitAddress(pxb, x, y, &byteIdx, &bitIdx);
    pxb->pixels[byteIdx] &= ~(0x01 << bitIdx);
}


void c8PixelBufferSetValue(struct C8PixelBuffer* pxb,
        uint8_t x,
        uint8_t y,
        uint8_t value)
{
    if (value)
        c8PixelBufferSet(pxb, x, y);
    else
        c8PixelBufferClear(pxb, x, y);
}

void c8PixelBufferScrollVert(struct C8PixelBuffer* pxb,
        uint8_t n)
{
    if (n == 0)
    {
        return;
    }

    uint16_t wrapDataSize = (n * pxb->lineWidth) >> 3;
    uint16_t byteIdx = 0;
    uint8_t bitIdx = 0;

    _getBitAddress(pxb, 0, n, &byteIdx, &bitIdx);

    memmove(&pxb->pixels[byteIdx],
            pxb->pixels,
            pxb->size - wrapDataSize);

    memset(pxb->pixels, 0x00, wrapDataSize);
}

void c8PixelBufferScrollHoriz(struct C8PixelBuffer* pxb,
        uint8_t direction)
{
    const uint8_t lines = pxb->size / pxb->lineWidth;
    const size_t n = 4;

    for (uint8_t line = 0; line < lines; ++line)
    {
        uint16_t lineByteIdx = 0;
        uint8_t lineBitIdx = 0;
        _getBitAddress(pxb, 0, line, &lineByteIdx, &lineBitIdx);

        if (C48_SCROLL_RIGHT == direction)
        {
            pxb->pixels[lineByteIdx] >>= n;
        }
        else
        {
            pxb->pixels[lineByteIdx] <<= n;
        }
    }
}

void c8PixelBufferDeinitialise(struct C8PixelBuffer* pxb)
{
    free(pxb->pixels);

    pxb->lineWidth = 0;
    pxb->size = 0;
    pxb->pixels = NULL;
}


#include "c8_pixel_buffer.h"
#include "c8_platform_common.h"
#include "c8_platform_sdl.h"

#include <SDL.h>
#include <stdlib.h>
#include <math.h>

typedef struct _SdlColor
{
    uint8_t r,g,b;
} SdlColor;


struct C8PlatformImpl
{
    // back buffer
    struct C8PixelBuffer pixelBuffer;

    // sdl stuff below
	SDL_Window *window;
	SDL_Renderer *renderer;
    SdlColor bgColor;
    SdlColor fgColor;

    // audio phase, freq, volume
    uint8_t beepOn;
    double audioPhase;
    double audioPhaseStep;
    int8_t audioVolume;
};

/* ========================================================================== */

static const uint16_t beepFrequency = 880;
static const uint32_t samplingFrequency = 44100;

/* ========================================================================== */

static void _platformSdlClearDisplay(struct C8Platform* platform);
static void _platformSdlSetColor(struct C8Platform* platform, SdlColor* c);
static void _platformSdlOpen(struct C8Platform* platform);
static void _platformSdlClose(struct C8Platform* platform);
static int8_t _platformSdlGetInput(struct C8Platform* platform);
static uint8_t _platformSdlDisplaySprite(
        struct C8Platform* platform,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n);
static void _platformSdlStep(struct C8Platform* platform);
static void _platformSdlBeep(struct C8Platform* platform, uint8_t beepOn);
static void _platformSdlGenerateSamples(void *udata, Uint8 *stream, int len);
static void _platformSdlOpenAudio(struct C8Platform* platform);
static void _platformSdlSetExtendedMode(struct C8Platform* platform,
        uint8_t isEnabled);

/* ========================================================================== */

static void _platformSdlSetColor(struct C8Platform* platform,
        SdlColor* c)
{
	SDL_SetRenderDrawColor(platform->impl->renderer,
            c->r, c->g, c->b, 0xff);
}

static void _platformSdlBlitPixels(struct C8Platform* platform)
{
    const uint8_t width = c8PlatformGetScreenWidth(platform);
    const uint8_t height = c8PlatformGetScreenHeight(platform);
    const uint8_t ps = c8PlatformGetPixelSize(platform);

    for (size_t y = 0; y < height; ++y)
    {
        for (size_t x = 0; x < width; ++x)
        {
            const uint16_t px = c8PlatformGetPixelCoords(platform, x);
            const uint16_t py = c8PlatformGetPixelCoords(platform, y);
            SDL_Rect rect = { px, py, ps, ps };

            const uint8_t val =
                c8PixelBufferGet(&platform->impl->pixelBuffer, x, y);

            _platformSdlSetColor(platform,
                    val ? &platform->impl->fgColor : &platform->impl->bgColor);

            SDL_RenderFillRect(platform->impl->renderer, &rect);
        }
    }

    SDL_RenderPresent(platform->impl->renderer);
}

static void _platformSdlOpen(struct C8Platform* platform)
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

    /*
     * Temporarily enable extended mode in order to get the most extreme
     * window coordinates.
     */
    platform->extendedMode = 1;

	platform->impl->window = SDL_CreateWindow("c8",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			c8PlatformGetPixelCoords(platform, platform->screenWidth),
			c8PlatformGetPixelCoords(platform, platform->screenHeight),
			SDL_WINDOW_SHOWN);

	platform->impl->renderer = SDL_CreateRenderer(
            platform->impl->window,
            -1,
            SDL_RENDERER_ACCELERATED);

    _platformSdlClearDisplay(platform);
    _platformSdlOpenAudio(platform);

    // fallback to chip8 mode
    platform->extendedMode = 0;
    _platformSdlSetExtendedMode(platform, 0);
}

static void _platformSdlOpenAudio(struct C8Platform* platform)
{
    platform->impl->audioPhaseStep = ((float)(2*M_PI*beepFrequency))/samplingFrequency;
    platform->impl->audioPhase = 0;
    platform->impl->audioVolume = 16;
    platform->impl->beepOn = 0;

	SDL_AudioSpec wanted;
    wanted.freq = samplingFrequency;
    wanted.format = AUDIO_U8;
    wanted.channels = 1;
    wanted.samples = 2048;
    wanted.callback = _platformSdlGenerateSamples;
    wanted.userdata = platform;

    if (SDL_OpenAudio(&wanted, NULL) < 0)
    {
        fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
        return;
    }
}

static void _platformSdlGenerateSamples(void *udata, uint8_t *stream, int len)
{
    struct C8Platform* platform = udata;
    for (int i = 0; i < len; i++)
    {
        stream[i] = 127 + platform->impl->audioVolume * sin(
                platform->impl->audioPhase);

        platform->impl->audioPhase += platform->impl->audioPhaseStep;
    }
}

static void _platformSdlClose(struct C8Platform* platform)
{
    platform->impl->beepOn = 0;
    SDL_CloseAudio();
	SDL_DestroyRenderer(platform->impl->renderer);
	SDL_DestroyWindow(platform->impl->window);
	SDL_Quit();
}

static int8_t _platformSdlGetInput(struct C8Platform* platform)
{
    SDL_Event event;
    uint8_t keymap[16] = {
        SDLK_0,
        SDLK_1,
        SDLK_2,
        SDLK_3,
        SDLK_4,
        SDLK_5,
        SDLK_6,
        SDLK_7,
        SDLK_8,
        SDLK_9,
        SDLK_a,
        SDLK_b,
        SDLK_c,
        SDLK_d,
        SDLK_e,
        SDLK_f,
    };

    while (SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        platform->input = C8_INPUT_QUIT;
                        break;

                    default:
                        for (size_t i = 0; i < sizeof(keymap); ++i)
                        {
                            if (event.key.keysym.sym == keymap[i])
                            {
                                platform->input = i;
                            }
                        }
                        break;

                }
                break;

            case SDL_KEYUP:
                platform->input = C8_INPUT_NONE;
                break;

            case SDL_QUIT:
                platform->input = C8_INPUT_QUIT;
                break;
        }
    }
    return platform->input;
}

static void _platformSdlClearDisplay(struct C8Platform* platform)
{
    c8PixelBufferClearAll(&platform->impl->pixelBuffer);
	_platformSdlSetColor(platform, &platform->impl->bgColor);
	SDL_RenderClear(platform->impl->renderer);
    SDL_RenderPresent(platform->impl->renderer);
}

static uint8_t _platformSdlDisplaySprite(
        struct C8Platform* platform,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n)
{
    const uint8_t isPixelCleared = c8PlatformCommonDisplaySprite(platform,
            &platform->impl->pixelBuffer,
            data,
            x,
            y,
            n);
    _platformSdlBlitPixels(platform);
    return isPixelCleared;
}

static void _platformSdlStep(struct C8Platform* platform)
{
    _platformSdlGetInput(platform);
}

static void _platformSdlBeep(struct C8Platform* platform, uint8_t beepOn)
{
    // if there is a change state - apply it
    if (platform->impl->beepOn ^ beepOn)
    {
        SDL_PauseAudio(!beepOn);
        platform->impl->beepOn = beepOn;
    }
}

static void _platformSdlScrollHoriz(struct C8Platform* platform,
        uint8_t direction)
{
    c8PixelBufferScrollHoriz(&platform->impl->pixelBuffer, direction);
    _platformSdlBlitPixels(platform);
}

static void _platformSdlScrollVert(struct C8Platform* platform,
        uint8_t n)
{
    if (n == 0)
    {
        return;
    }

    c8PixelBufferScrollVert(&platform->impl->pixelBuffer, n);
    _platformSdlBlitPixels(platform);
}

static void _platformSdlSetExtendedMode(struct C8Platform* platform,
        uint8_t isEnabled)
{
    c8PixelBufferSetHalfMode(&platform->impl->pixelBuffer,
            !isEnabled);
}

uint8_t c8PlatformImplInitialize(struct C8Platform* platform)
{
    platform->impl = malloc(sizeof(struct C8PlatformImpl));
    platform->platformOpen = _platformSdlOpen;
    platform->platformClose = _platformSdlClose;
    platform->getInput = _platformSdlGetInput;
    platform->clearDisplay = _platformSdlClearDisplay;
    platform->displaySprite = _platformSdlDisplaySprite;
    platform->platformStep = _platformSdlStep;
    platform->beepControl = _platformSdlBeep;
    platform->scrollHoriz = _platformSdlScrollHoriz;
    platform->scrollVert = _platformSdlScrollVert;
    platform->setExtendedMode = _platformSdlSetExtendedMode;

    SdlColor fg = { .r = 0x60, .g = 0xff, .b = 0x10};
    SdlColor bg = { .r = 0x2f, .g = 0x2f, .b = 0x2f};

    platform->impl->fgColor = fg;
    platform->impl->bgColor = bg;

    c8PixelBufferInitialise(&platform->impl->pixelBuffer,
            platform->screenWidth,
            platform->screenHeight);

    return 0;
}

void c8PlatformImplDeInitialize(struct C8Platform* platform)
{
    if (platform->impl)
    {
        c8PixelBufferDeinitialise(&platform->impl->pixelBuffer);
        free(platform->impl);
    }
}

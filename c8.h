#ifndef _C8_H_
#define _C8_H_

#include "c8_platform.h"

#include <stdint.h>

#define C8_MEMSIZE 4096
#define C8_STACKSIZE 16
#define C8_FONT_H 5
#define C48_FONT_H 10
#define C8_RESERVED_MEMSIZE 512

enum C8ExecMode
{
    Running = 0,
    WaitInput,
    Halted
};

typedef struct _C8
{
    union
    {
        // main system memory
        uint8_t memory[C8_MEMSIZE];

        struct
        {
            /// font data for all 16 hexadecimal digits
            uint8_t hexFont[0x10 * C8_FONT_H];

            /// super chip8 10x10 character digit font
            uint8_t hexFont48[0x0a * C48_FONT_H];

            /// V{0..F} registers
            uint8_t vReg[16];

            /// system stack
            uint16_t stack[C8_STACKSIZE];

            /// index register
            uint16_t iReg;

            /// program counter
            uint16_t pcReg;

            /// stack pointer
            uint8_t spReg;

            /// delay timer
            uint8_t dtReg;

            /// sound timer
            uint8_t stReg;

            /// chip8/schip8
            uint8_t isEmu48;

            /// original boot address
            uint16_t bootAddress;

            /// how many cycles execute before ticking the timers
            uint16_t cyclesPerTimerTick;

            /// virtual machine execution state
            /// in wait mode, x is the pointer to destination register
            /// ---- | ----
            ///    x | mode
            uint8_t status;

            /// platform implementation
            struct C8Platform* platform;
        } c;
    };

} C8;

uint8_t c8Initialize(C8* ctx,
        struct C8Platform* platform,
        uint16_t bootAddress,
        uint16_t cyclesPerTimer,
        uint8_t isEmu48);

void c8Load(C8* ctx, const char* romPath, uint16_t loadAddress);
void c8Run(C8* ctx);
void c8Deinitialize(C8* ctx);


#endif

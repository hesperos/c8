#ifndef C8_PLATFORM_NULL_H_
#define C8_PLATFORM_NULL_H_

#include "c8_platform.h"

uint8_t c8PlatformImplInitialize(struct C8Platform* platform);
void c8PlatformImplDeInitialize(struct C8Platform* platform);


#endif /* C8_PLATFORM_NULL_H_ */

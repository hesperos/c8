#include <check.h>
#include <stdlib.h>
#include <time.h>

// module under test
#include "../c8_pixel_buffer.h"

START_TEST(test_ifAllocatesAndDeallocatesMemory)
{
    struct C8PixelBuffer pxb;
    const int w = 10;
    const int h = 20;

    memset(&pxb, 0x00, sizeof(struct C8PixelBuffer));
    ck_assert(pxb.pixels == NULL);

    c8PixelBufferInitialise(&pxb, w, h);

    ck_assert(pxb.pixels != NULL);
    ck_assert(pxb.size != 0);
    ck_assert(pxb.lineWidth == w);
    ck_assert(pxb.size == (w*h)/8);

    c8PixelBufferDeinitialise(&pxb);
    ck_assert(pxb.pixels == NULL);
}
END_TEST

START_TEST(test_ifSetsCorrectBits)
{
    struct C8PixelBuffer pxb;
    const int w = 40;
    const int h = 50;
    c8PixelBufferInitialise(&pxb, w, h);

    c8PixelBufferSet(&pxb, 1, 0);
    c8PixelBufferSet(&pxb, 2, 0);

    // check bit pattern so far
    ck_assert_int_eq(pxb.pixels[0], 0x06);

    c8PixelBufferSet(&pxb, 8, 0);
    c8PixelBufferSet(&pxb, 9, 0);

    // check bit pattern again
    ck_assert_int_eq(pxb.pixels[0], 0x06);
    ck_assert_int_eq(pxb.pixels[1], 0x03);

    c8PixelBufferSet(&pxb, 13, 2);
    c8PixelBufferSet(&pxb, 34, 23);

    // check bytes once more
    ck_assert_int_eq(pxb.pixels[(13 + 2*w) / 8],
            (0x01 << 5));
    ck_assert_int_eq(pxb.pixels[(34 +  23*w) / 8],
            (0x01 << (34 % 8)));

    c8PixelBufferDeinitialise(&pxb);
}
END_TEST

START_TEST(test_ifGetReturnsCorrectValues)
{
    struct C8PixelBuffer pxb;
    const int w = 40;
    const int h = 50;
    int n = 1024;

    c8PixelBufferInitialise(&pxb, w, h);

    while (n--)
    {
        int rx = rand() % w;
        int ry = rand() % h;

        c8PixelBufferSet(&pxb, rx, ry);
        ck_assert_int_eq(c8PixelBufferGet(&pxb, rx, ry), 0x01);

        c8PixelBufferClear(&pxb, rx, ry);
        ck_assert_int_eq(c8PixelBufferGet(&pxb, rx, ry), 0x00);
    }

    c8PixelBufferDeinitialise(&pxb);
}
END_TEST

START_TEST(test_ifScrollsContentsHorizontally)
{

}
END_TEST

START_TEST(test_ifScrollsContentsVertically)
{

}
END_TEST


Suite * pixelBufferTestSuite(void)
{
    Suite* s;
    TCase* tcCore;

    s = suite_create("PixelBufferTestSuite");

    /* Core test case */
    tcCore = tcase_create("Core");

    tcase_add_test(tcCore,
            test_ifAllocatesAndDeallocatesMemory);
    tcase_add_test(tcCore,
            test_ifSetsCorrectBits);
    tcase_add_test(tcCore,
            test_ifGetReturnsCorrectValues);
    tcase_add_test(tcCore,
            test_ifScrollsContentsHorizontally);
    tcase_add_test(tcCore,
            test_ifScrollsContentsVertically);

    suite_add_tcase(s, tcCore);
    return s;
}


int main(void)
{
    srand(time(NULL));

    Suite* s = pixelBufferTestSuite();
    SRunner* sr = srunner_create(s);
    int nFailures = 0;

    srunner_run_all(sr, CK_NORMAL);
    nFailures = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nFailures ? EXIT_FAILURE : EXIT_SUCCESS;
}

#include "c8_platform_common.h"

#include <string.h>
#include <stdio.h>

uint8_t c8PlatformCommonDisplaySprite(struct C8Platform* platform,
        struct C8PixelBuffer* pxb,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n)
{
    uint8_t isPixelCleared = 0;
    const uint8_t isXdraw = c8PlatformIsXDraw(platform, n);
    uint8_t spriteHeight = isXdraw ? 16 : n;
    uint8_t spriteWidth = isXdraw ? 16 : 8;
    const uint8_t screenWidth = c8PlatformGetScreenWidth(platform);
    const uint8_t screenHeight = c8PlatformGetScreenHeight(platform);

    // clip to screen dimensions
    x = x & (screenWidth - 1);
    y = y & (screenHeight - 1);

    // prevent out of screen rendering on X axis
    if (screenWidth <= (spriteWidth + x))
        spriteWidth = screenWidth - x;

    // prevent out of screen rendering on Y axis
    if (screenHeight <= (spriteHeight + y))
        spriteHeight = screenHeight - y;

    for (size_t i = 0; i < spriteHeight; i++)
    {
        for (size_t c = 0; c < spriteWidth; c++)
        {
            const uint8_t cx = x + c;
            const uint8_t iy = y + i;
            const uint8_t isLitUp = c8PixelBufferGet(pxb, cx, iy);

            const uint8_t bitIdx = c & 0x07;
            const uint8_t byteIdx = (i << isXdraw) + (c >> 0x03);
            const uint8_t shouldLitUp = (data[byteIdx] & (0x80 >> bitIdx)) != 0;
            const uint8_t willLitUp = (isLitUp ^ shouldLitUp) != 0x00;

            if (shouldLitUp && isLitUp)
                isPixelCleared = 1;

            c8PixelBufferSetValue(pxb, cx, iy, willLitUp);
        }
    }

    return isPixelCleared;
}

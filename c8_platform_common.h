#ifndef C8_PLATFORM_COMMON_H_
#define C8_PLATFORM_COMMON_H_

#include "c8_platform.h"
#include "c8_pixel_buffer.h"

#include <stdlib.h>

uint8_t c8PlatformCommonDisplaySprite(
        struct C8Platform* platform,
        struct C8PixelBuffer* pxb,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n);

#endif /* C8_PLATFORM_COMMON_H_ */

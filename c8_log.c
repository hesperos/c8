#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static void _printMsg(const char* severity, const char* format, va_list args)
{
    char msgBuff[1024] = {0x00};
    vsnprintf(msgBuff, sizeof(msgBuff), format, args);
    fprintf(stderr, "%s: %s\n", severity, msgBuff);
}

void c8PrintWarn(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("WARN", format, args);
    va_end(args);
}

void c8PrintInfo(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("INFO", format, args);
    va_end(args);
}

void c8PrintError(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("ERROR", format, args);
    va_end(args);
}


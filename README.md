# c8 is a C chip8 machine

Simple chip8 emulator written in C. Just for fun.

Some screens:

![Screenshot sdl](screenshots/sdl_cars.png)

![Screenshot sdl](screenshots/sdl_blinky.png)

![Screenshot ncurses](screenshots/ncurses_puzzle.png)


# Building

c8 supports a couple of different platforms:
- sdl2
- ncurses (experimental)
- null (no gfx, no input - just for debugging)

Platform selection should be done with the `C8_PLATFORM` cmake option:

    $ mkdir bld
    $ cd bld
    $ cmake -DC8_PLATFORM=sdl -DCMAKE_BUILD_TYPE=Release ..
    $ make

It's possible to use the binaries from the build directory. There are no
additional dynamic library dependencies besides mentioned. Optionally it can be
installed the standard way:

    $ make install

# Development

Visit the c8asm directory to familiarise yourself with the provided chip8
assembly - c8asm. Comes with a set of example programs.

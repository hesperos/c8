#include "c8.h"
#include "c8_platform.h"
#include "c8_log.h"

#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

// RPL user flags simulation from HP48
static uint8_t hp48Rpl[0x10] = {0x00};

static const uint8_t c8Font[0x10 * C8_FONT_H] = {
	0xf0, 0x90, 0x90, 0x90, 0xf0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xf0, 0x10, 0xf0, 0x80, 0xf0, // 2
	0xf0, 0x10, 0xf0, 0x10, 0xf0, // 3
	0x90, 0x90, 0xf0, 0x10, 0x10, // 4
	0xf0, 0x80, 0xf0, 0x10, 0xf0, // 5
	0xf0, 0x80, 0xf0, 0x90, 0xf0, // 6
	0xf0, 0x10, 0x20, 0x40, 0x40, // 7
	0xf0, 0x90, 0xf0, 0x90, 0xf0, // 8
	0xf0, 0x90, 0xf0, 0x10, 0xf0, // 9
	0xf0, 0x90, 0xf0, 0x90, 0x90, // A
	0xe0, 0x90, 0xe0, 0x90, 0xe0, // B
	0xf0, 0x80, 0x80, 0x80, 0xf0, // C
	0xe0, 0x90, 0x90, 0x90, 0xe0, // D
	0xf0, 0x80, 0xf0, 0x80, 0xf0, // E
	0xf0, 0x80, 0xf0, 0x80, 0x80, // F
};

static const uint8_t c48Font[0x0a * C48_FONT_H] = {
    0x3c, 0x7e, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0x7e, 0x3c, // 0
    0x18, 0x38, 0x58, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3c, // 1
    0x3e, 0x7f, 0xc3, 0x06, 0x0c, 0x18, 0x30, 0x60, 0xff, 0xff, // 2
    0x3c, 0x7e, 0xc3, 0x03, 0x0e, 0x0e, 0x03, 0xc3, 0x7e, 0x3c, // 3
    0x06, 0x0e, 0x1e, 0x36, 0x66, 0xc6, 0xff, 0xff, 0x06, 0x06, // 4
    0xff, 0xff, 0xc0, 0xc0, 0xfc, 0xfe, 0x03, 0xc3, 0x7e, 0x3c, // 5
    0x3e, 0x7c, 0xc0, 0xc0, 0xfc, 0xfe, 0xc3, 0xc3, 0x7e, 0x3c, // 6
    0xff, 0xff, 0x03, 0x06, 0x0c, 0x18, 0x30, 0x60, 0x60, 0x60, // 7
    0x3c, 0x7e, 0xc3, 0xc3, 0x7e, 0x7e, 0xc3, 0xc3, 0x7e, 0x3c, // 8
    0x3c, 0x7e, 0xc3, 0xc3, 0x7f, 0x3f, 0x03, 0x03, 0x3e, 0x7c, // 9
};

/* ========================================================================== */

static void c8ResetCtx(C8* ctx);

/* ========================================================================== */

#ifndef NDEBUG
static void dumpMemory(C8* ctx)
{
    const size_t rowSize = 16;
    for (size_t n = 0; n < C8_MEMSIZE; n += rowSize)
    {
        printf("%04lx: ", n);
        for (size_t i = 0; i < rowSize; i++)
        {
            printf("%02x ", (unsigned)(ctx->memory[n + i]));
        }
        printf("\n");
    }
}

static void dumpMachine(C8* ctx)
{
    printf("v[3-0]: %02x %02x %02x %02x\n",
            ctx->c.vReg[3], ctx->c.vReg[2], ctx->c.vReg[1], ctx->c.vReg[0]);
    printf("v[7-4]: %02x %02x %02x %02x\n",
            ctx->c.vReg[7], ctx->c.vReg[6], ctx->c.vReg[5], ctx->c.vReg[4]);
    printf("v[b-8]: %02x %02x %02x %02x\n",
            ctx->c.vReg[11], ctx->c.vReg[10], ctx->c.vReg[9], ctx->c.vReg[8]);
    printf("v[f-c]: %02x %02x %02x %02x\n",
            ctx->c.vReg[15], ctx->c.vReg[14], ctx->c.vReg[13], ctx->c.vReg[12]);
    printf("pc: %04x i: %04x\n", ctx->c.pcReg, ctx->c.iReg);
    printf("dt: %02x st: %02x\n", ctx->c.dtReg, ctx->c.stReg);
}
#endif

static void c8InstallFont(C8* ctx)
{
    memcpy(ctx->c.hexFont, c8Font, sizeof(c8Font));
    memcpy(ctx->c.hexFont48, c48Font, sizeof(c48Font));
}

static uint8_t c8IsKeyPressed(C8* ctx, uint8_t key)
{
    return ctx->c.platform->input == ctx->c.vReg[key];
}

static void c8HandleTimers(C8* ctx)
{
    if (ctx->c.dtReg > 0)
    {
        ctx->c.dtReg--;
    }

    if (ctx->c.stReg > 0)
    {
        ctx->c.stReg--;
    }

    ctx->c.platform->beepControl(ctx->c.platform, ctx->c.stReg != 0);
}

static uint8_t c8StatusGetX(C8* ctx)
{
    return ctx->c.status >> 4;
}

static uint8_t c8StatusGetMode(C8* ctx)
{
    return ctx->c.status & 0x0f;
}

static void c8StatusSet(C8* ctx, uint8_t x, uint8_t mode)
{
    ctx->c.status = (x << 4) | (mode & 0x0f);
}

static void c8UpdateInputRegister(C8* ctx)
{
    ctx->c.vReg[c8StatusGetX(ctx)] = ctx->c.platform->input;
}

struct C8OpCodeDetails
{
    uint16_t instructionGroup;
    uint16_t nnn;
    uint8_t kk;
    uint8_t x;
    uint8_t y;
    uint8_t n;
};

static void decodeInstruction(uint16_t instr, struct C8OpCodeDetails* opCodeDetails)
{
    // pre-calculate the decoded instruction bits
    opCodeDetails->instructionGroup = instr & 0xf000;
    opCodeDetails->nnn = instr & 0xfff;
    opCodeDetails->kk = instr & 0xff;
    opCodeDetails->x = (instr & 0xf00) >> 8;
    opCodeDetails->y = (instr & 0xf0) >> 4;
    opCodeDetails->n = (instr & 0x0f);
}

static void handle0000(C8* ctx, struct C8OpCodeDetails* details)
{
    switch(details->nnn)
    {
        case 0x0e0:
            ctx->c.platform->clearDisplay(ctx->c.platform);
            break;

        case 0x0ee:
            ctx->c.pcReg = ctx->c.stack[ctx->c.spReg-- % C8_STACKSIZE];
            break;

        // SChip8
        case 0x0fb:
            if (ctx->c.isEmu48)
                ctx->c.platform->scrollHoriz(ctx->c.platform, C48_SCROLL_RIGHT);
            break;

        // SChip8
        case 0x0fc:
            if (ctx->c.isEmu48)
                ctx->c.platform->scrollHoriz(ctx->c.platform, C48_SCROLL_LEFT);
            break;

        // SChip8
        case 0x0fd:
            if (ctx->c.isEmu48)
            {
                c8PrintInfo("resetting system ...");
                c8ResetCtx(ctx);
            }
            break;

        // SChip8
        case 0x0fe:
        case 0x0ff:
            if (ctx->c.isEmu48)
            {
                ctx->c.platform->extendedMode = details->n == 0xf;
                ctx->c.platform->setExtendedMode(ctx->c.platform,
                        ctx->c.platform->extendedMode);
            }
            break;

        default:
            // SChip8
            if (ctx->c.isEmu48 && details->y == 0x0c)
            {
                ctx->c.platform->scrollVert(ctx->c.platform, details->n);
            }
            else
            {
                // ctx->c.pcReg = details->nnn;
            }
            break;
    }
}

static void handle1000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg = details->nnn;
}

static void handle2000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.stack[++ctx->c.spReg % C8_STACKSIZE] = ctx->c.pcReg;
    ctx->c.pcReg = details->nnn;
}

static void handle3000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg += ctx->c.vReg[details->x] == details->kk ? 2 : 0;
}

static void handle4000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg += ctx->c.vReg[details->x] != details->kk ? 2 : 0;
}

static void handle5000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg += ctx->c.vReg[details->x] == ctx->c.vReg[details->y] ? 2 : 0;
}

static void handle6000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.vReg[details->x] = details->kk;
}

static void handle7000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.vReg[details->x] += details->kk;
}

static void handle8000(C8* ctx, struct C8OpCodeDetails* details)
{
    uint16_t tmp = 0x00;
    switch(details->n)
    {
        case 0x00: ctx->c.vReg[details->x] = ctx->c.vReg[details->y]; break;
        case 0x01: ctx->c.vReg[details->x] |= ctx->c.vReg[details->y]; break;
        case 0x02: ctx->c.vReg[details->x] &= ctx->c.vReg[details->y]; break;
        case 0x03: ctx->c.vReg[details->x] ^= ctx->c.vReg[details->y]; break;
        case 0x04:
            tmp = ctx->c.vReg[details->x] + ctx->c.vReg[details->y];
            ctx->c.vReg[details->x] = (tmp & 0xff);
            ctx->c.vReg[0x0f] = tmp > 0xff ? 0x01 : 0x00;
            break;

        case 0x05:
            ctx->c.vReg[0x0f] = ctx->c.vReg[details->y] > ctx->c.vReg[details->x] ?
                0x00 : 0x01;
            ctx->c.vReg[details->x] -= ctx->c.vReg[details->y];
            break;

        case 0x06:
            ctx->c.vReg[0x0f] = ctx->c.vReg[details->x] & 0x01;
            ctx->c.vReg[details->x] >>= 1;
            break;

        case 0x07:
            ctx->c.vReg[0x0f] = ctx->c.vReg[details->x] > ctx->c.vReg[details->y] ?
                0x00 : 0x01;
            ctx->c.vReg[details->x] = ctx->c.vReg[details->y] - ctx->c.vReg[details->x];
            break;

        case 0x0e:
            ctx->c.vReg[0x0f] = ctx->c.vReg[details->x] >> 7;
            ctx->c.vReg[details->x] <<= 1;
            break;
    }
}

static void handle9000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg += ctx->c.vReg[details->x] != ctx->c.vReg[details->y] ? 2 : 0;
}

static void handlea000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.iReg = details->nnn;
}

static void handleb000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.pcReg = details->nnn + ctx->c.vReg[0];
}

static void handlec000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.vReg[details->x] = rand() & details->kk;
}

static void handled000(C8* ctx, struct C8OpCodeDetails* details)
{
    ctx->c.vReg[0x0f] = ctx->c.platform->displaySprite(ctx->c.platform,
            &ctx->memory[ctx->c.iReg],
            ctx->c.vReg[details->x] % ctx->c.platform->screenWidth,
            ctx->c.vReg[details->y] % ctx->c.platform->screenHeight,
            details->n);
}

static void handlee000(C8* ctx, struct C8OpCodeDetails* details)
{
    switch (details->kk)
    {
        case 0x9e:
            ctx->c.pcReg += c8IsKeyPressed(ctx, details->x) ? 2 : 0;
            break;
        case 0xa1:
            ctx->c.pcReg += c8IsKeyPressed(ctx, details->x) ? 0 : 2;
            break;
    }
}

static void handlef000(C8* ctx, struct C8OpCodeDetails* details)
{
    switch (details->kk)
    {
        case 0x07: ctx->c.vReg[details->x] = ctx->c.dtReg; break;
        case 0x0a: c8StatusSet(ctx, details->x, WaitInput); break;
        case 0x15: ctx->c.dtReg = ctx->c.vReg[details->x]; break;
        case 0x18: ctx->c.stReg = ctx->c.vReg[details->x]; break;
        case 0x1e:
            ctx->c.iReg += ctx->c.vReg[details->x];
            ctx->c.vReg[0x0f] = ctx->c.iReg > 0xfff ? 0x01 : 0x00;
            break;
        case 0x29:
            ctx->c.iReg = ctx->c.vReg[details->x]*C8_FONT_H;
            break;

        // SChip8
        case 0x30:
            if (ctx->c.isEmu48)
                ctx->c.iReg = sizeof(ctx->c.hexFont) +
                    ctx->c.vReg[details->x % 0x0a]*C48_FONT_H;
            break;

        case 0x33:
            ctx->memory[ctx->c.iReg] = ctx->c.vReg[details->x] / 100;
            ctx->memory[ctx->c.iReg + 1] = ctx->c.vReg[details->x] / 10 % 10;
            ctx->memory[ctx->c.iReg + 2] = ctx->c.vReg[details->x] % 10;
            break;
        case 0x55:
            memcpy(&ctx->memory[ctx->c.iReg], ctx->c.vReg, details->x + 1);
            ctx->c.iReg += details->x + 1;
            break;
        case 0x65:
            memcpy(ctx->c.vReg, &ctx->memory[ctx->c.iReg], details->x + 1);
            ctx->c.iReg += details->x + 1;
            break;

        // SChip8
        case 0x75:
            memcpy(hp48Rpl, ctx->c.vReg, details->x + 1);
            ctx->c.iReg += details->x + 1;
            break;

        case 0x85:
            memcpy(ctx->c.vReg, hp48Rpl, details->x + 1);
            ctx->c.iReg += details->x + 1;
            break;

#ifndef NDEBUG
        // diagnostics
        case 0xff:
            switch(details->x)
            {
                case 0x00:
                    dumpMachine(ctx);
                    break;

                case 0x01:
                    dumpMemory(ctx);
                    break;
            }
            break;
#endif
    }

}


static void (*c8DecodeTable[0x10])(C8*, struct C8OpCodeDetails*) = {
    handle0000, handle1000, handle2000, handle3000,
    handle4000, handle5000, handle6000, handle7000,
    handle8000, handle9000, handlea000, handleb000,
    handlec000, handled000, handlee000, handlef000,
};

static void c8Step(C8* ctx)
{
    const uint16_t instr =
        (ctx->memory[ctx->c.pcReg] << 8) | ctx->memory[ctx->c.pcReg + 1];
    ctx->c.pcReg += 2;
    struct C8OpCodeDetails details;

    decodeInstruction(instr, &details);
    c8DecodeTable[details.instructionGroup>>12](ctx, &details);
}

static void c8ResetCtx(C8* ctx)
{
    struct C8Platform* platform = ctx->c.platform;
    const uint16_t bootAddress = ctx->c.bootAddress;
    const uint16_t cyclesPerTimerTick = ctx->c.cyclesPerTimerTick;
    const uint8_t isEmu48 = ctx->c.isEmu48;

    memset(ctx->memory, 0x00, sizeof(ctx->memory));
    c8InstallFont(ctx);
    c8StatusSet(ctx, 0, Running);
    ctx->c.platform = platform;
    ctx->c.bootAddress = bootAddress;
    ctx->c.cyclesPerTimerTick = cyclesPerTimerTick;
    ctx->c.pcReg = bootAddress;
    ctx->c.isEmu48 = isEmu48;
}

uint8_t c8Initialize(C8* ctx,
        struct C8Platform* platform,
        uint16_t bootAddress,
        uint16_t cyclesPerTimer,
        uint8_t isEmu48)
{
    if (sizeof(ctx->c) > C8_RESERVED_MEMSIZE)
    {
        c8PrintError("Context size is too big: %d!", sizeof(C8));
        return 1;
    }

    srand(time(NULL));
    ctx->c.platform = platform;
    ctx->c.cyclesPerTimerTick = cyclesPerTimer;
    ctx->c.bootAddress = bootAddress;
    ctx->c.isEmu48 = isEmu48;
    c8ResetCtx(ctx);
    ctx->c.platform->platformOpen(ctx->c.platform);
    return 0;
}

void c8Load(C8* ctx, const char* romPath, uint16_t loadAddress)
{
    FILE* f = fopen(romPath, "r");
    uint8_t buf[256];
    size_t offset = loadAddress;
    while (!feof(f) && offset < C8_MEMSIZE)
    {
        size_t n = fread(buf, 1, sizeof(buf), f);
        memcpy(&ctx->memory[offset], buf, n);
        offset += n;
    }
    fclose(f);
}

static double timeDiff(struct timespec* t1, struct timespec* t2)
{
    double secs = t1->tv_sec - t2->tv_sec;
    secs += (t1->tv_nsec - t2->tv_nsec) * 1e-9;
	return secs;
}

void c8Run(C8* ctx)
{
    const int loopDelayUs = 100;
    const int timerFreq = 50;

    struct timespec ts1, ts2;
    int n = 0;
    int mode = 0;

    // initialize timestamps
    clock_gettime(CLOCK_MONOTONIC, &ts1);
    ts2 = ts1;

    while (Halted != (mode = c8StatusGetMode(ctx)))
    {
        clock_gettime(CLOCK_MONOTONIC, &ts2);
        double tDiff = timeDiff(&ts2, &ts1);
        n =  tDiff * timerFreq;

        if (n)
        {
            clock_gettime(CLOCK_MONOTONIC, &ts1);
            while (n--)
            {
                int cpuIterations = ctx->c.cyclesPerTimerTick;
                while (cpuIterations--)
                {
                    if (WaitInput == mode)
                    {
                        if (C8_INPUT_NONE != ctx->c.platform->input)
                        {
                            c8UpdateInputRegister(ctx);
                            c8StatusSet(ctx, 0, Running);
                        }
                    }
                    else
                    {
                        c8Step(ctx);
                    }
                    ctx->c.platform->platformStep(ctx->c.platform);
                }

                c8HandleTimers(ctx);

                if (C8_INPUT_QUIT == ctx->c.platform->input)
                {
                    c8StatusSet(ctx, 0, Halted);
                    break;
                }
            }
        }
        usleep(loopDelayUs);
    }
}

void c8Deinitialize(C8* ctx)
{
    ctx->c.platform->platformClose(ctx->c.platform);
}

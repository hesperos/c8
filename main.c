#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "c8.h"

void usage()
{
    fprintf(stderr, "c8 [ OPTIONS ] -r romfile\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-c <cycles>\t-\tcycles per timer refresh (default: 16)\n");
    fprintf(stderr, "\t-b <addr>\t-\tboot address (default: 512)\n");
    fprintf(stderr, "\t-l <addr>\t-\tload address (default: 512)\n");
    fprintf(stderr, "\t-s\t\t-\tenable super chip8 instructions (default: no)\n");
}

extern char* optarg;
extern int optind;

int main(int argc, char *argv[])
{
    C8 c8;
    struct C8Platform platform;

    const char* options = "l:c:b:r:hd:s";
    int opt = 0;
    char romFilePath[256] = { 0x00 };
    uint16_t bootAddress = 0x200;
    uint16_t loadAddress = 0x200;
    uint16_t cyclesPerTimer = 16;
    uint8_t isEmu48 = 0;

#ifdef USE_PLATFORM_SDL
    const uint8_t pixelSize = 16;
#elif defined USE_PLATFORM_NCURSES
    const uint8_t pixelSize = 2;
#endif

    while (-1 != (opt = getopt(argc, argv, options)))
    {
        switch (opt)
        {
            case 'r':
                strncpy(romFilePath, optarg, sizeof(romFilePath));
                break;

            case 'b':
                bootAddress = atoi(optarg);
                break;

            case 'l':
                loadAddress = atoi(optarg);
                break;

            case 'c':
                cyclesPerTimer = atoi(optarg);
                break;

            case 's':
                isEmu48 = 1;
                break;

            default:
            case 'h':
                usage();
                exit(EXIT_SUCCESS);
                break;
        }
    }

    if (strlen(romFilePath) == 0)
    {
        usage();
        exit(EXIT_FAILURE);
    }

    if (!c8PlatformInitialize(&platform,
                C48_SCREEN_W, C48_SCREEN_H, pixelSize) &&
        !c8Initialize(&c8, &platform, bootAddress, cyclesPerTimer, isEmu48))
    {
        c8Load(&c8, romFilePath, loadAddress);
        c8Run(&c8);
        c8Deinitialize(&c8);
        c8PlatformDeInitialize(&platform);
    }
    return 0;
}

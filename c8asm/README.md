# c8asm

c8asm is a chip8 assembler as described by the Cowgod's chip8 reference at: [Cowgod's
chip8 technical reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)

## c8asm syntax

```
program :=
    [ label: ] <mnemonic> [ arg ] [ ',' arg ] [ ',' arg ]
```

White characters are ignorred.

### Comments:

Begin with ';' character. I.e:

    ; this is a comment
    ld v1, v2 ;this is a comment as well

### Literals:

c8asm accepts hex and decimal literals declared as follows:

    ld v1, #10 ; load 10 (in decimal) to v1
    ld v1, $10 ; load 10 (in hex) to v1

### Labels

Labels can be used to simplify address calculation:

    my_label:
        ld v1, #10
        jp my_label

## c8asm mnemonics

### Nullary mnemonics:

- 00e0 - cls        - clear screen,
- 00ee - ret        - return from function (pc = address from stack).

### Unary mnemonics:

- 0nnn - sys addr   - jump to address nnn (pc = nnn),
- 1nnn - jp addr    - jump to address nnn (pc = nnn),
- 2nnn - call addr  - call function at nnn (return address pushed to stack),
- ex9e - skp Vx     - skip next instruction if key in Vx pressed,
- exa1 - sknp Vx    - skip next instruction if key in Vx not pressed,
- fx0a - ldk Vx     - halt execution and wait for key press. Store key in Vx,
- fx29 - ldf Vx     - load I with pointer to a font sprite for hex digit in Vx,
- fx33 - ldb Vx     - store BCD representation of Vx at addresses: I, I+1, I+2
- fx55 - ldi Vx     - store registers V0 through Vx in memory starting at I,
- fx65 - ldv Vx     - read registers V0 through Vx from memory starting at I,
- 8xy6 - shr Vx     - Vx = Vx >> 1, Vf = LSB of Vx,
- 8xye - shl Vx     - Vx = Vx << 1, Vf = MSB of Vx.

### Binary mnemonics:

- 3xkk - se Vx, byte    - skip next instruction if Vx = kk,
- 5xy0 - se Vx, Vy      - skip next instruction if Vx = Vy,
- 4xkk - sne Vx, byte   - skip next instruction if Vx != kk,
- 9xy0 - sne Vx, Vy     - skip next instruction if Vx != Vy,
- 7xkk - add Vx, byte   - set Vx = Vx + kk,
- 8xy4 - add Vx, Vy     - set Vx = Vx + Vy, Vf = result > 255 ? 1 : 0,
- fx1e - add i, Vx      - set I = I + Vx,
- cxkk - rnd Vx, byte   - set Vx = random byte AND kk,
- bnnn - jp v0, addr    - jump to location nnn + V0 (pc = nnn + v0),
- annn - ld i, addr     - set I = nnn,
- 6xkk - ld Vx, byte    - set Vx = kk,
- 8xy0 - ld Vx, Vy      - set Vx = Vy,
- fx07 - ld Vx, dt      - loads the value from delay timer register to Vx,
- fx15 - ld dt, Vx      - loads Vx with the value from the delay timer register,
- fx18 - ld st, Vx      - loads sound timer with the value from Vx
- 8xy1 - or Vx, Vy      - set Vx = Vx OR Vy,
- 8xy2 - and Vx, Vy     - set Vx = Vx AND Vy,
- 8xy3 - xor Vx, Vy     - set Vx = Vx XOR Vy,
- 8xy5 - sub Vx, Vy     - set Vx = Vx - Vy, Vf = Vx > Vy ? 1 : 0,
- 8xy7 - subn Vx, Vy    - set Vx = Vy - Vx, Vf = Vy > Vx ? 1 : 0.

### Ternary mnemonics:

- dxyn - drw Vx, Vy, nibble - display n-byte sprite starting at memory location
I at (Vx, Vy), set VF = collision. In extended mode if n == 0, the sprite size
is fixed to 16x16, otherwise the sprite size if 8xn.

### Super Chip8 mnemonics:

- fx33 - ldf48		- load 16x10 font address for digit in Vx into I
- 00fc - scrhl48	- horizontally scroll left by 4 pixels,
- 00fb - scrhr48	- horizontally scroll right by 4 pixels,
- 00cn - scrv48		- vertically scroll down by n lines,
- 00ff - exten48	- enable extended mode, 128x64,
- 00fe - extdi48	- disable extended mode, 64x32,
- 00fd - rst48		- reset the machine.

## c8asm directives

- .org _address_ - code following the directive will be emmitted starting from
this address

- .byte _bytes_ ... - emmits bytes at current location

- .str "string data" - emits bytes for corresponding ASCII characters in the
string

- .reserve _nbytes_ - moves the instruction pointer by _nbytes_ forward -
should be used to reserve some space in the program memory

- .bits "bit string" - declares a byte defined with a bitstring. The string may
have arbitrarilly length of multiples of 8.

 - Declaring a 0xff byte:
    .bits "********"

 - Declaring 0x55 byte:
    .bits " * * * *"

 - Declaring 16-bits:
    .bits "* *** *** *** **" (0xbbbb)

- .alias register identifier - creates a register alias. Ie:

    .alias v0 coordX
    .alias v1 coordY

These can be used later on with any mnemonic accepting a register:

    ld coordX, #10

%{
#include "c8vm.h"
#include "c8parser.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
%}

%option bison-locations
%option bison-bridge
%option yylineno

%%

v[0-9a-f] {
    yylval->intval = strtoul(yytext + 1, 0, 16) & 0x0f;
    return T_RID;
}

dt { yylval->intval = C8VM_SREG_OFFSET + 0; return T_SRID_DT; }
st { yylval->intval = C8VM_SREG_OFFSET + 1; return T_SRID_ST; }
i  { yylval->intval = C8VM_SREG_OFFSET + 2; return T_SRID_I; }

#[0-9]+ {
    yylval->intval = strtoul(yytext + 1, 0, 10) & 0x0fff;
    return T_DEC_LITERAL;
}

\$[0-9a-f]+ {
    yylval->intval = strtoul(yytext + 1, 0, 16) & 0x0fff;
    return T_HEX_LITERAL;
}

cls 	{ return T_CLS; }
ret 	{ return T_RET; }
sys 	{ return T_SYS; }
jp 		{ return T_JP; }
call 	{ return T_CALL; }
skp 	{ return T_SKP; }
sknp 	{ return T_SKNP; }
ldf 	{ return T_LDF; }
ldf48 	{ return T_LDF48; }
ldb 	{ return T_LDB; }
ldi 	{ return T_LDI; }
ldv 	{ return T_LDV; }
ldk 	{ return T_LDK; }
rnd 	{ return T_RND; }
se 		{ return T_SE; }
or 		{ return T_OR; }
and 	{ return T_AND; }
xor 	{ return T_XOR; }
add 	{ return T_ADD; }
sub 	{ return T_SUB; }
shr 	{ return T_SHR; }
subn 	{ return T_SUBN; }
sne 	{ return T_SNE; }
ld 		{ return T_LD; }
shl 	{ return T_SHL; }
drw 	{ return T_DRW; }
scrhl48 { return T_SCRHL48; }
scrhr48 { return T_SCRHR48; }
scrv48  { return T_SCRV48; }
exten48 { return T_EXTEN48; }
extdi48 { return T_EXTDI48; }
rst48   { return T_RST48; }

.(byte|org|str|reserve|bits|alias|dumpmachine|dumpmemory) {
    struct DirMap {
        const char* directive;
        int directiveId;
    } directives[] = {
        { "byte",    T_DIR_BYTE },
        { "org",     T_DIR_ORG },
        { "str",     T_DIR_STR },
        { "reserve", T_DIR_RESERVE },
        { "bits",    T_DIR_BITS },
        { "alias",   T_DIR_ALIAS },
        { "dumpmachine", T_DIR_DMP_MACHINE },
        { "dumpmemory", T_DIR_DMP_MEMORY },
        { NULL,      0x00 },
    };
    struct DirMap *ptr = directives;

    yylval->strval = strdup(yytext + 1);
    while (ptr->directive)
    {
        if (0 == strcmp(yytext + 1, ptr->directive))
        {
            return ptr->directiveId;
        }
        ptr++;
    }

    // TODO signal error
    return 0x00;
}

[a-zA-Z][a-zA-Z0-9]* {
    yylval->strval = strdup(yytext);
    return T_ID;
}


\".*\" {
    yylval->strval = strdup(yytext + 1);
    yylval->strval[strlen(yylval->strval) - 1] = '\0';
    return T_STRING;
}

,|: {
    return yytext[0];
}

";".* {
    // this is a comment
}

[ \t\n]+ {
    // ignore white spaces
}

%%

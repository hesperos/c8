#include "c8vm.h"
#include "log.h"

#include <stdlib.h>
#include <string.h>

#define C8VM_MIN(a,b) ((a) < (b) ? a : b)
#define C8VM_MAX(a,b) ((a) > (b) ? a : b)

/* ========================================================================== */

static void setIp(struct c8vm* c8vm, uint16_t newIp);
static uint16_t incrementIp(struct c8vm* c8vm);
static void adjustIp(struct c8vm* c8vm);
static void _c8vmFreeAliases(struct c8vm* ctx);

static size_t _c8vmCalculateNargs(struct c8vmLabelPatchArgs* args);
static struct c8vmLabelPatchArgs* _c8vmGetArg(
        struct c8vmLabelPatchArgs* args, size_t n);

static uint8_t _c8vmEmitSysUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip);
static uint8_t _c8vmEmitJpUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip);
static uint8_t _c8vmEmitCallUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip);
static uint8_t _c8vmEmitJpBinary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip);
static uint8_t _c8vmEmitLdBinary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip);

static uint8_t _c8vmResolveLabel(
        struct c8vm* ctx,
        struct c8vmLabelPatch* patch,
        uint8_t labelIdx,
        uint8_t (*emit)(struct c8vm* ctx, struct c8vmLabelPatch*, uint16_t ip));
static uint8_t _c8vmCompileLabelPatch(struct c8vm* ctx,
        struct c8vmLabelPatch* patch);

static void _c8vmFreeLabelPatch(struct c8vmLabelPatch* patch);
static void _c8vmFreeLabelPatches(struct c8vmLabelPatches* patches);
static void _c8vmFreeLabelPatches(struct c8vmLabelPatches* patches);

/* ========================================================================== */

static void setIp(struct c8vm* c8vm, uint16_t newIp)
{
    c8PrintTrace("%s():%d newIp: %d\n", __FUNCTION__, __LINE__, newIp);
    if (newIp >= C8VM_ROM_MAXSIZE)
    {
        c8PrintError("instruction out side of memory boundary: %x/%x",
                newIp, C8VM_ROM_MAXSIZE);
        return;
    }
    c8vm->ip = newIp;
    c8vm->endAddress = C8VM_MAX(c8vm->endAddress, c8vm->ip);
}

static uint16_t incrementIp(struct c8vm* c8vm)
{
    c8PrintTrace("%s():%d\n", __FUNCTION__, __LINE__);
    uint16_t oldIp = c8vm->ip;
    setIp(c8vm, c8vm->ip + 1);
    return oldIp;
}

static void adjustIp(struct c8vm* c8vm)
{
    if (c8vm->ip % 2)
    {
        c8PrintWarn("alligning to ip@0x%x to 16-bit boundary", c8vm->ip);
        incrementIp(c8vm);
    }
}

/* ========================================================================== */

void c8vmInitialize(struct c8vm* c8vm, uint16_t loadAddress)
{
    c8vm->loadAddress = loadAddress;
    c8vm->ip = loadAddress;
    c8vm->endAddress = c8vm->ip;
    c8vm->labels = NULL;
    c8vm->patches = NULL;

    memset(c8vm->memory, 0x00, sizeof(c8vm->memory));
    memset(&c8vm->regAliases, 0x00, sizeof(c8vm->regAliases));
}

void c8vmDeinitialize(struct c8vm* c8vm)
{
    _c8vmFreeLabelPatches(c8vm->patches);
    _c8vmFreeAliases(c8vm);
    struct c8vmLabels* ptr = c8vm->labels;
    while (ptr != NULL)
    {
        struct c8vmLabels* nxt = ptr->nxt;
        free(ptr->label.label);
        free(ptr);
        ptr = nxt;
    }
}

void c8vmFlush(struct c8vm* c8vm, FILE* output)
{
    size_t chunkSize = 0x400;
    size_t index = c8vm->loadAddress;
    while (index < c8vm->endAddress)
    {
        size_t writeSize = C8VM_MIN(chunkSize, c8vm->endAddress - index);
        index += fwrite(&c8vm->memory[index],
                sizeof(uint8_t), writeSize, output);
    }
    c8PrintInfo("written %d bytes", index - c8vm->loadAddress);
}

/* ========================================================================== */

uint8_t c8vmInstallLabel(struct c8vm* ctx, char* label)
{
    struct c8vmLabels* newHead = malloc(sizeof(struct c8vmLabels));
    newHead->label.label = label;
    newHead->label.ip = ctx->ip;
    newHead->nxt = ctx->labels;
    ctx->labels = newHead;
    c8PrintInfo("creating label %s@%04x", label, ctx->ip);
    return 0x00;
}

uint8_t c8vmGetLabel(struct c8vm* ctx, const char* label, uint16_t* ip)
{
    struct c8vmLabels* ptr = ctx->labels;
    uint8_t rv = 0x01;

    while (ptr)
    {
        if (0 == strcmp(label, ptr->label.label))
        {
            *ip = ptr->label.ip;
            rv = 0x00;
            break;
        }
        ptr = ptr->nxt;
    }

    return rv;
}

/* ========================================================================== */

void c8vmEmitWord(struct c8vm* ctx, uint16_t word)
{
    adjustIp(ctx);
    c8PrintTrace("%s():%d emitting word: %04x@%04x\n",
            __FUNCTION__, __LINE__,
            word, ctx->ip);
    ctx->memory[incrementIp(ctx)] = word >> 8;
    ctx->memory[incrementIp(ctx)] = word & 0xff;
}

void c8vmEmitBytes(struct c8vm* ctx, struct c8vmLiteralSlice* slice)
{
    if (slice->len == 0)
    {
        c8PrintWarn("expecting at least one argument for [byte] directive");
    }
    else
    {
        for (size_t n = 0; n < slice->len; ++n)
        {
            const uint16_t ip = incrementIp(ctx);
            c8PrintTrace("%s():%d emitting byte: %02x@%04x\n",
                    __FUNCTION__, __LINE__, slice->literal[n], ip);
            ctx->memory[ip] = slice->literal[n];
        }
    }
}

void c8vmEmitString(struct c8vm* ctx, const char* str)
{
    for (size_t i = 0; i < strlen(str); i++)
    {
        const uint16_t ip = incrementIp(ctx);
        c8PrintTrace("%s():%d emitting character: %c@%04x\n",
                __FUNCTION__, __LINE__, str[i], ip);
        ctx->memory[ip] = str[i];
    }

    // terminate string
    ctx->memory[incrementIp(ctx)] = 0x00;
}

void c8vmEmitBits(struct c8vm* ctx, const char* str)
{
    uint16_t ip = 0;
    uint8_t byte = 0;

    for (size_t i = 0; i < strlen(str); i++)
    {
        uint8_t bitIndex = i % 8;

        if (str[i] != ' ')
        {
            byte |= (1 << bitIndex);
            c8PrintTrace("%s():%d setting bit: %d -> %02x\n",
                    __FUNCTION__, __LINE__, bitIndex, byte);
        }

        if (0 == bitIndex && (0 != i))
        {
            ip = incrementIp(ctx);
            c8PrintTrace("%s():%d emitting byte: %02x@%04x\n",
                    __FUNCTION__, __LINE__, byte, ip);
            ctx->memory[ip] = byte;
            byte = 0x00;
        }
    }

    // terminate string
    ctx->memory[incrementIp(ctx)] = 0x00;
}

void c8vmSetIp(struct c8vm* ctx, uint16_t ip)
{
    setIp(ctx, ip);
}

void c8vmMoveIp(struct c8vm* ctx, uint16_t ip)
{
    setIp(ctx, ctx->ip + ip);
}

/* ========================================================================== */

static size_t _c8vmCalculateNargs(struct c8vmLabelPatchArgs* args)
{
    size_t nArgs = 0;
    struct c8vmLabelPatchArgs* ptr = args;
    while (ptr)
    {
        nArgs++;
        ptr = ptr->nxt;
    }
    return nArgs;
}

static struct c8vmLabelPatchArgs* _c8vmGetArg(
        struct c8vmLabelPatchArgs* args, size_t n)
{
    while (n--)
    {
        args = args->nxt;
    }
    return args;
}

static uint8_t _c8vmEmitSysUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip)
{
    c8vmEmitWord(ctx, 0x0000 | ip);
    return 0x00;
}

static uint8_t _c8vmEmitJpUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip)
{
    c8vmEmitWord(ctx, 0x1000 | ip);
    return 0x00;
}

static uint8_t _c8vmEmitCallUnary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip)
{
    c8vmEmitWord(ctx, 0x2000 | ip);
    return 0x00;
}

static uint8_t _c8vmEmitJpBinary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip)
{
    c8vmEmitWord(ctx, 0xb000 | ip);
    return 0x00;
}

static uint8_t _c8vmEmitLdBinary(struct c8vm* ctx,
        struct c8vmLabelPatch* patch, uint16_t ip)
{
    c8vmEmitWord(ctx, 0xa000 | ip);
    return 0x00;
}

static uint8_t _c8vmResolveLabel(
        struct c8vm* ctx,
        struct c8vmLabelPatch* patch,
        uint8_t labelIdx,
        uint8_t (*emit)(struct c8vm* ctx, struct c8vmLabelPatch*, uint16_t ip))
{
    uint8_t rv = 0x00;
    uint16_t ip = 0x0000;
    const uint16_t prefix = 0x0000;

    struct c8vmLabelPatchArgsLabel* larg =
        (struct c8vmLabelPatchArgsLabel *)_c8vmGetArg(patch->args, labelIdx);

    if (NULL == larg)
    {
        c8PrintError("label missing");
        return 0x01;
    }

    ctx->ip = patch->ip;
    if (c8vmGetLabel(ctx, larg->label, &ip))
    {
        c8PrintInfo("label not defined (possibly will be defined later)");
        c8vmEmitWord(ctx, 0x0000);
        rv = 0x01;
    }
    else
    {
        rv = emit(ctx, patch, ip);
    }

    if (!rv)
    {
        c8PrintInfo("label [%s@%04x] patched @%04x",
                larg->label,
                ip,
                patch->ip);
    }

    return rv;
}

static uint8_t _c8vmCompileLabelPatch(struct c8vm* ctx,
        struct c8vmLabelPatch* patch)
{
    uint16_t ip = 0;
    uint8_t rv = 0x01;
    struct Mnemo
    {
        const char* mnemo;
        size_t nArgs;
        size_t labelIdx;
        uint8_t (*emit)(struct c8vm* ctx, struct c8vmLabelPatch*, uint16_t);
    } mnemos[] = {
        { "sys",  1, 0, _c8vmEmitSysUnary },
        { "jp",   1, 0, _c8vmEmitJpUnary },
        { "call", 1, 0, _c8vmEmitCallUnary },
        { "jp",   2, 1, _c8vmEmitJpBinary },
        { "ld",   2, 1, _c8vmEmitLdBinary },
        { NULL,   0, 0, NULL },
    };

    struct Mnemo* ptr = mnemos;
    while (ptr->mnemo && ptr->emit)
    {
        if (0 == strcmp(ptr->mnemo, patch->mnemo) && ptr->nArgs == patch->nArgs)
        {
            c8PrintInfo("processing mnemo: %s", ptr->mnemo);
            rv = _c8vmResolveLabel(ctx, patch, ptr->labelIdx, ptr->emit);
            break;
        }
        ptr++;
    }
    return rv;
}

static void _c8vmFreeLabelPatch(struct c8vmLabelPatch* patch)
{
    free(patch->mnemo);
    struct c8vmLabelPatchArgs* ptr = patch->args;

    // delete arguments
    while(ptr)
    {
        switch(ptr->type)
        {
            case C8VM_ARG_LABEL:
                free(((struct c8vmLabelPatchArgsLabel *)ptr)->label);
                break;

            default:
                break;
        }
        struct c8vmLabelPatchArgs* toFree = ptr;
        ptr = ptr->nxt;
        free(toFree);
    }
}

static void _c8vmFreeLabelPatches(struct c8vmLabelPatches* patches)
{
    struct c8vmLabelPatches* ptr = patches;

    // delete arguments
    while(ptr)
    {
        _c8vmFreeLabelPatch(&ptr->patch);
        struct c8vmLabelPatches* toFree = ptr;
        ptr = ptr->nxt;
        free(toFree);

    }
}

uint8_t c8vmEmitLabelPatches(struct c8vm* ctx)
{
    uint8_t rv = 0x00;
    struct c8vmLabelPatches* patches = ctx->patches;

    while(patches)
    {
        if (_c8vmCompileLabelPatch(ctx, &patches->patch))
        {
            c8PrintError("failed label patching");
            rv = 0x01;
            break;
        }
        patches = patches->nxt;
    }

    return rv;
}

uint8_t c8vmEmitWordWithLabels(
        struct c8vm* ctx,
        const char* mnemo,
        struct c8vmLabelPatchArgs* args)
{
    size_t nArgs = _c8vmCalculateNargs(args);
    uint8_t rv = 0x00;

    struct c8vmLabelPatches* patch =
        malloc(sizeof(struct c8vmLabelPatches));

    if (NULL == patch)
    {
        c8PrintError("Unable to allocate label patched instruction");
        return 0x01;
    }

    patch->patch.ip = ctx->ip;
    patch->patch.mnemo = strdup(mnemo);
    patch->patch.args = args;
    patch->patch.nArgs = nArgs;
    patch->nxt = NULL;

    if ((rv = _c8vmCompileLabelPatch(ctx, &patch->patch)))
    {
        // failed - add to list and attempt later
        patch->nxt = ctx->patches;
        ctx->patches = patch;
    }
    else
    {
        _c8vmFreeLabelPatch(&patch->patch);
        free(patch);
    }

    return rv;
}

void _c8vmFreeAliases(struct c8vm* ctx)
{
    for (size_t i = 0;
            i < sizeof(ctx->regAliases.alias)/sizeof(ctx->regAliases.alias[0]);
            ++i)
    {
        if (ctx->regAliases.alias[i])
        {
            free(ctx->regAliases.alias[i]);
        }
    }
}

void c8vmCreateAlias(struct c8vm* ctx, uint8_t registerIndex, char* alias)
{
    uint8_t regIdx = registerIndex % 16;
    if (ctx->regAliases.alias[regIdx])
    {
        free(ctx->regAliases.alias[regIdx]);
    }

    ctx->regAliases.alias[regIdx] = alias;
}

uint8_t c8vmGetRegIdxFromAlias(struct c8vm* ctx, char* alias)
{
    for (size_t i = 0;
            i < sizeof(ctx->regAliases.alias)/sizeof(ctx->regAliases.alias[0]);
            ++i)
    {
        if (ctx->regAliases.alias[i] &&
                (0 == strcmp(alias, ctx->regAliases.alias[i])))
        {
            return i;
        }
    }

    c8PrintError("unable to resolve alias: [%s]", alias);
    return 0xff;
}

uint8_t c8vmIsAliasForbidden(char* alias)
{
    /*
     * Contains a list of forbidden aliases - intented initially for reserved
     * words, but these will be taken care of by the lexer - thankfully
     */
    const char *reservedKeywords[] = {

    };

    for (size_t i = 0;
            i < sizeof(reservedKeywords)/sizeof(reservedKeywords[0]);
            ++i)
    {
        if (0 == strcmp(alias, reservedKeywords[i]))
        {
            return 1;
        }
    }

    return 0;
}

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static unsigned g_verbosity = 0;

static void _printMsg(const char* severity,
        unsigned logLevel,
        const char* format,
        va_list args)
{
    char msgBuff[1024] = {0x00};
    if (g_verbosity >= logLevel)
    {
        vsnprintf(msgBuff, sizeof(msgBuff), format, args);
        fprintf(stderr, "%s: %s\n", severity, msgBuff);
    }
}

void c8PrintTrace(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("TRACE", 3, format, args);
    va_end(args);
}

void c8PrintInfo(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("INFO", 2, format, args);
    va_end(args);
}

void c8PrintWarn(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("WARN", 1, format, args);
    va_end(args);
}

void c8PrintError(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    _printMsg("ERROR", 0, format, args);
    va_end(args);
}

void c8SetVerbosity(unsigned verbosity)
{
    g_verbosity = verbosity;
}


#ifndef LOG_H_
#define LOG_H_

void c8SetVerbosity(unsigned verbosity);
void c8PrintTrace(const char* format, ...);
void c8PrintInfo(const char* format, ...);
void c8PrintWarn(const char* format, ...);
void c8PrintError(const char* format, ...);

#endif /* LOG_H_ */

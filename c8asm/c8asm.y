%{

#include "log.h"
#include "c8vm.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct YYLTYPE;
union YYSTYPE;
extern int yylineno;

int yylex(union YYSTYPE* yylval_param,
    struct YYLTYPE* yylloc_param);

int yyerror(struct YYLTYPE* yylloc_param,
    struct c8vm* ctx,
    const char* msg);

%}

%locations
%define api.pure full
%define parse.error verbose

%parse-param { struct c8vm* ctx }

%union
{
    uint32_t intval;
    char *strval;
    struct c8vmLiteralSlice literalsval;
}

%start program

/* mnemonics: nullary */
%token T_CLS T_RET T_RST48 T_EXTEN48 T_EXTDI48 T_SCRHL48 T_SCRHR48
/* mnemonics: unary */
%token T_SYS T_JP T_CALL T_SKP T_SKNP T_LDF T_LDB T_LDI T_LDV T_LDK
T_SCRV48 T_LDF48
/* mnemonics: binary */
%token T_SE T_SNE T_OR T_AND T_XOR T_ADD T_SUB T_SUBN T_SHR T_SHL T_LD T_RND
/* mnemonics: ternary */
%token T_DRW

/* register definitions */
%token <intval> T_RID T_SRID_DT T_SRID_ST T_SRID_I
/* literal definitions */
%token <intval> T_DEC_LITERAL T_HEX_LITERAL
/* labels, strings */
%token <strval> T_ID T_STRING
/* directives */
%token <strval> T_DIR_BYTE T_DIR_ORG T_DIR_STR
T_DIR_RESERVE T_DIR_BITS T_DIR_ALIAS T_DIR_DMP_MACHINE T_DIR_DMP_MEMORY

/* non-terminal type definitions */
%type<intval> literal
%type<literalsval> literals
%type<intval> regid

%%

program:
     %empty
     | statements
        { c8vmEmitLabelPatches(ctx); }
     ;
statements: statement | statements statement;
statement:
     nullary
     | unary
     | binary
     | ternary
     | directive
     | label
     ;
literal: T_HEX_LITERAL | T_DEC_LITERAL;

literals:
     literal {
        struct c8vmLiteralSlice slice;
        slice.size = 8;
        slice.len = 0;
        slice.literal = malloc(sizeof(*slice.literal) * slice.size);
        slice.literal[slice.len++] = $1;
        $$ = slice;
     }
     | literals literal {
        if ($1.len >= $1.size)
        {
            size_t newSize = $1.size * 2;
            $1.literal = realloc($1.literal, sizeof(*$1.literal) * newSize);
            $1.size = newSize;
        }

        $1.literal[$1.len++] = $2;
        $$ = $1;
     }
     ;

nullary:
     T_CLS          { c8vmEmitWord(ctx, 0x00e0); }
     | T_RET        { c8vmEmitWord(ctx, 0x00ee); }
     | T_RST48      { c8vmEmitWord(ctx, 0x00fd); }
     | T_EXTEN48    { c8vmEmitWord(ctx, 0x00ff); }
     | T_EXTDI48    { c8vmEmitWord(ctx, 0x00fe); }
     | T_SCRHL48    { c8vmEmitWord(ctx, 0x00fc); }
     | T_SCRHR48    { c8vmEmitWord(ctx, 0x00fb); }
     ;

regid:
     T_RID          { $$ = $1; }
     | T_ID
        {
            uint8_t regIndex = c8vmGetRegIdxFromAlias(ctx, $1);
            free($1);
            if (0xff == regIndex)
            {
                YYABORT;
            }
            $$ = regIndex;
        }
     ;

unary:
     T_SYS literal
        { c8vmEmitWord(ctx, 0x0000 | $2); }
     | T_SYS T_ID
        {
            struct c8vmLabelPatchArgsLabel* labelArg =
                malloc(sizeof(struct c8vmLabelPatchArgsLabel));
            labelArg->label = $2;
            labelArg->base.type = C8VM_ARG_LABEL;
            labelArg->base.nxt = NULL;
            c8vmEmitWordWithLabels(ctx, "sys", (struct c8vmLabelPatchArgs *)labelArg);
        }
     | T_JP literal
        { c8vmEmitWord(ctx, 0x1000 | $2); }
     | T_JP T_ID
        {
            struct c8vmLabelPatchArgsLabel* labelArg =
                malloc(sizeof(struct c8vmLabelPatchArgsLabel));
            labelArg->label = $2;
            labelArg->base.type = C8VM_ARG_LABEL;
            labelArg->base.nxt = NULL;
            c8vmEmitWordWithLabels(ctx, "jp", (struct c8vmLabelPatchArgs *)labelArg);
        }
     | T_CALL literal
        { c8vmEmitWord(ctx, 0x2000 | $2); }
     | T_CALL T_ID
        {
            struct c8vmLabelPatchArgsLabel* labelArg =
                malloc(sizeof(struct c8vmLabelPatchArgsLabel));
            labelArg->label = $2;
            labelArg->base.type = C8VM_ARG_LABEL;
            labelArg->base.nxt = NULL;
            c8vmEmitWordWithLabels(ctx, "call", (struct c8vmLabelPatchArgs *)labelArg);
        }
     | T_SKP regid
        { c8vmEmitWord(ctx, ((0xe0 | $2) << 8 | 0x9e)); }
     | T_SKNP regid
        { c8vmEmitWord(ctx, ((0xe0 | $2) << 8 | 0xa1)); }
     | T_LDK regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x0a)); }
     | T_SCRV48 literal
        { c8vmEmitWord(ctx, 0x00c0 | $2 & 0x0f); }
     | T_LDF regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x29)); }
     | T_LDF48 regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x30)); }
     | T_LDB regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x33)); }
     | T_LDI regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x55)); }
     | T_LDV regid
        { c8vmEmitWord(ctx, ((0xf0 | $2) << 8 | 0x65)); }
     | T_SHR regid
        { c8vmEmitWord(ctx, ((0x80 | $2) << 8 | 0x06)); }
     | T_SHL regid
        { c8vmEmitWord(ctx, ((0x80 | $2) << 8 | 0x0e)); }
     ;

binary:
     T_SE regid ',' literal
        { c8vmEmitWord(ctx, (0x30 | $2) << 8 | $4); }
     | T_SE regid ',' regid
        { c8vmEmitWord(ctx, (0x50 | $2) << 8 | ($4 << 4)); }
     | T_SNE regid ',' literal
        { c8vmEmitWord(ctx, (0x40 | $2) << 8 | $4); }
     | T_SNE regid ',' regid
        { c8vmEmitWord(ctx, (0x90 | $2) << 8 | ($4 << 4)); }
     | T_ADD regid ',' literal
        { c8vmEmitWord(ctx, (0x70 | $2) << 8 | $4); }
     | T_ADD regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x04); }
     | T_ADD T_SRID_I ',' regid
        { c8vmEmitWord(ctx, (0xf0 | $4) << 8 | 0x1e); }
     | T_RND regid ',' literal
        { c8vmEmitWord(ctx, (0xc0 | $2) << 8 | $4); }
     | T_JP regid ',' literal
        { c8vmEmitWord(ctx, 0xb000 | $4); }
     | T_JP regid ',' T_ID
        {
            struct c8vmLabelPatchArgsRegIdx* rArg =
                malloc(sizeof(struct c8vmLabelPatchArgsRegIdx));
            struct c8vmLabelPatchArgsLabel* lArg =
                malloc(sizeof(struct c8vmLabelPatchArgsLabel));

            rArg->regIdx = $2;
            rArg->base.type = C8VM_ARG_REGIDX;
            rArg->base.nxt = (struct c8vmLabelPatchArgs *)lArg;

            lArg->label = $4;
            lArg->base.type = C8VM_ARG_LABEL;
            lArg->base.nxt = NULL;

            c8vmEmitWordWithLabels(ctx, "sys", (struct c8vmLabelPatchArgs *)rArg);
        }
     | T_LD T_SRID_I ',' literal
        { c8vmEmitWord(ctx, 0xa000 | $4); }
     | T_LD T_SRID_I ',' T_ID
        {
            struct c8vmLabelPatchArgsRegIdx* rArg =
                malloc(sizeof(struct c8vmLabelPatchArgsRegIdx));
            struct c8vmLabelPatchArgsLabel* lArg =
                malloc(sizeof(struct c8vmLabelPatchArgsLabel));

            rArg->regIdx = $2;
            rArg->base.type = C8VM_ARG_REGIDX;
            rArg->base.nxt = (struct c8vmLabelPatchArgs *)lArg;

            lArg->label = $4;
            lArg->base.type = C8VM_ARG_LABEL;
            lArg->base.nxt = NULL;

            c8vmEmitWordWithLabels(ctx, "ld", (struct c8vmLabelPatchArgs *)rArg);
        }
     | T_LD regid ',' literal
        { c8vmEmitWord(ctx, (0x60 | $2) << 8 | $4); }
     | T_LD regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4)); }
     | T_LD regid ',' T_SRID_DT
        { c8vmEmitWord(ctx, (0xf0 | $2) << 8 | 0x07); }
     | T_LD T_SRID_DT ',' regid
        { c8vmEmitWord(ctx, (0xf0 | $2) << 8 | 0x15); }
     | T_LD T_SRID_ST ',' regid
        { c8vmEmitWord(ctx, (0xf0 | $2) << 8 | 0x18); }
     | T_OR regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x01); }
     | T_AND regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x02); }
     | T_XOR regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x03); }
     | T_SUB regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x05); }
     | T_SUBN regid ',' regid
        { c8vmEmitWord(ctx, (0x80 | $2) << 8 | ($4 << 4) | 0x07); }
     ;

ternary: T_DRW regid ',' regid ',' literal
       { c8vmEmitWord(ctx, (0xd0 | $2) << 8 | ($4 << 4 | $6)); }
     ;

directive:
     T_DIR_BYTE literals
        { c8vmEmitBytes(ctx, &$2); free($1); free($2.literal); }
     | T_DIR_ORG literal
        { c8vmSetIp(ctx, $2); free($1); }
     | T_DIR_STR T_STRING
        { c8vmEmitString(ctx, $2); free($1); free($2); }
     | T_DIR_RESERVE literal
        { c8vmMoveIp(ctx, $2); free($1); }
     | T_DIR_BITS T_STRING
        { c8vmEmitBits(ctx, $2); free($1); free($2); }
     | T_DIR_ALIAS T_RID T_ID
        {
            free($1);
            if (c8vmIsAliasForbidden($3))
            {
                free($3);
                YYABORT;
            }
            c8vmCreateAlias(ctx, $2, $3);
        }
     | T_DIR_DMP_MACHINE
        { c8vmEmitWord(ctx, 0xf0ff); free($1); }
     | T_DIR_DMP_MEMORY
        { c8vmEmitWord(ctx, 0xf1ff); free($1); }
     ;

label:
     T_ID ':'
        { c8vmInstallLabel(ctx, $1); }

%%

int yyerror(struct YYLTYPE* yylloc_param,
    struct c8vm* ctx,
    const char* msg)
{
    c8PrintError("%s @(%d:%d - %d:%d)", msg,
            yylineno,
            yylloc_param->first_column,
            yylineno,
            yylloc_param->last_column);
}

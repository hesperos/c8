#ifndef C8VM_H_
#define C8VM_H_

#include <stdio.h>
#include <stdint.h>

#define C8VM_DEFAULT_LOAD_ADDRESS 0x200
#define C8VM_ROM_MAXSIZE 4096
#define C8VM_SREG_OFFSET 0x80

struct c8vmLiteralSlice
{
    uint32_t *literal;
    size_t size;
    size_t len;
};

struct c8vmLabel
{
    char* label;
    uint16_t ip;
};

struct c8vmLabels
{
    struct c8vmLabel label;
    struct c8vmLabels* nxt;
};

enum C8VmLabelPatchArgType
{
    C8VM_ARG_REGIDX,
    C8VM_ARG_LABEL,
    C8VM_ARG_INVALID,
};

struct c8vmLabelPatchArgs
{
    enum C8VmLabelPatchArgType type;
    struct c8vmLabelPatchArgs* nxt;
};

struct c8vmLabelPatchArgsRegIdx
{
    struct c8vmLabelPatchArgs base;
    uint8_t regIdx;
};

struct c8vmRegisterAliases
{
    char *alias[16];
};

struct c8vmLabelPatchArgsLabel
{
    struct c8vmLabelPatchArgs base;
    char* label;
};

struct c8vmLabelPatch
{
    uint16_t ip;
    char* mnemo;
    size_t nArgs;
    struct c8vmLabelPatchArgs* args;
};

struct c8vmLabelPatches
{
    struct c8vmLabelPatch patch;
    struct c8vmLabelPatches* nxt;
};

struct c8vm
{
    /// the lowest address used by any data definition or code in the program
    uint16_t loadAddress;

    /// the highest address used by any data definition or code
    uint16_t endAddress;

    /// instruction pointer
    uint16_t ip;

    /// memory that will be dumped to a file
    uint8_t memory[C8VM_ROM_MAXSIZE];

    /// memory location labels
    struct c8vmLabels* labels;

    /// register aliases
    struct c8vmRegisterAliases regAliases;

    /// intructions requiring deferred label patching
    struct c8vmLabelPatches* patches;
};

void c8vmInitialize(struct c8vm* c8vm, uint16_t loadAddress);
void c8vmDeinitialize(struct c8vm* c8vm);
void c8vmFlush(struct c8vm* c8vm, FILE* output);
uint8_t c8vmIsAliasForbidden(char* alias);
uint8_t c8vmGetRegIdxFromAlias(struct c8vm* ctx, char* alias);
void c8vmCreateAlias(struct c8vm* ctx, uint8_t registerIndex, char* alias);

uint8_t c8vmInstallLabel(struct c8vm* ctx, char* label);
uint8_t c8vmGetLabel(struct c8vm* ctx, const char* label, uint16_t* ip);
uint8_t c8vmEmitWordWithLabels(struct c8vm* ctx, const char* mnemo, struct c8vmLabelPatchArgs* args);
uint8_t c8vmEmitLabelPatches(struct c8vm* ctx);

void c8vmEmitWord(struct c8vm* ctx, uint16_t word);
void c8vmEmitBytes(struct c8vm* ctx, struct c8vmLiteralSlice* slice);
void c8vmEmitString(struct c8vm* ctx, const char* str);
void c8vmEmitBits(struct c8vm* ctx, const char* str);

void c8vmSetIp(struct c8vm* ctx, uint16_t ip);
void c8vmMoveIp(struct c8vm* ctx, uint16_t ip);

#endif /* C8VM_H_ */

find_package(BISON REQUIRED)
find_package(FLEX REQUIRED)

BISON_TARGET(c8asmParser
    c8asm.y
    ${CMAKE_CURRENT_BINARY_DIR}/c8parser.c
)

FLEX_TARGET(c8asmScanner
    c8asm.l
    ${CMAKE_CURRENT_BINARY_DIR}/c8lexer.c
    DEFINES_FILE ${CMAKE_CURRENT_BINARY_DIR}/c8lexer.h
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${FLEX_INCLUDE_DIRS})
include_directories(${BISON_INCLUDE_DIRS})

add_executable(c8asm
    main.c
    c8vm.c
    log.c
    ${BISON_c8asmParser_OUTPUTS}
    ${FLEX_c8asmScanner_OUTPUTS}
)
target_link_libraries(c8asm
    ${BISON_LIBRARIES}
    ${FLEX_LIBRARIES}
)

install(TARGETS c8asm RUNTIME DESTINATION bin)

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "log.h"
#include "c8vm.h"
#include "c8parser.h"
#include "c8lexer.h"

void usage()
{
    const char* version = "1.1";
    fprintf(stderr, "c8asm ver: %s [ OPTIONS ] <srcfile>\n", version);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-o <output>\t-\tOutput file\n");
    fprintf(stderr, "\t-v <verbosity>\t-\tVerbosity level: 1-3 (default: 1)\n");
}

extern char* optarg;
extern int optind;

int main(int argc, char *argv[])
{
    int opt = 0;
    const char *options = "o:h";
    char *inputFilePath = NULL;
    char outputFilePath[1024] = "./a.out";
    FILE *inputFile = NULL;
    FILE *outputFile = NULL;
    struct c8vm data;
    int hasFailed = 0;
    unsigned verbosity = 1;

    while (-1 != (opt = getopt(argc, argv, options)))
    {
        switch (opt)
        {
            case 'o':
                strncpy(outputFilePath, optarg, sizeof(outputFilePath));
                break;

            case 'v':
                verbosity = strtoul(optarg, NULL, 10);
                break;

            default:
                c8PrintInfo("unknown option: %c", opt);
                usage();
                exit(EXIT_SUCCESS);
                break;
        }
    }

    inputFilePath = argv[optind];
    if (optind == argc || 0 == strlen(inputFilePath))
    {
        c8PrintError("no source file provided");
        usage();
        exit(EXIT_FAILURE);
    }

    if (NULL == (inputFile = fopen(inputFilePath, "r")))
    {
        c8PrintError("unable to open input file: %s", inputFilePath);
        exit(EXIT_FAILURE);
    }

    if (NULL == (outputFile = fopen(outputFilePath, "w+")))
    {
        fclose(inputFile);
        c8PrintError("unable to open output file: %s", outputFilePath);
        exit(EXIT_FAILURE);
    }

    yyin = inputFile;
    c8SetVerbosity(verbosity);
    c8vmInitialize(&data, C8VM_DEFAULT_LOAD_ADDRESS);

    do
    {
        if (yyparse(&data))
        {
            c8PrintError("parse error, terminating...");
            hasFailed = 1;
            break;
        }
    } while(!feof(inputFile));

    if (!hasFailed)
    {
        c8vmFlush(&data, outputFile);
    }

    yylex_destroy();
    c8vmDeinitialize(&data);
    fclose(inputFile);
    fclose(outputFile);
    return EXIT_SUCCESS;
}

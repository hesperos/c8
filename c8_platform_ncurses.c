#include "c8_pixel_buffer.h"
#include "c8_platform_common.h"
#include "c8_platform_ncurses.h"

#include <curses.h>
#include <stdlib.h>

struct C8PlatformImpl
{
    struct C8PixelBuffer pixelBuffer;

    uint8_t keyRepeat;
    uint8_t beepOn;
};

/* ========================================================================== */

static void _platformNcursesSetExtendedMode(struct C8Platform* platform,
        uint8_t isEnabled);

/* ========================================================================== */

static void _platformNcursesBlitPixels(struct C8Platform* platform)
{
    const uint8_t width = c8PlatformGetScreenWidth(platform);
    const uint8_t height = c8PlatformGetScreenHeight(platform);
    const uint8_t ps = c8PlatformGetPixelSize(platform);
    const uint8_t pixelOn = '#';
    const uint8_t pixelOff = ' ';

    for (size_t y = 0; y < height; ++y)
    {
        for (size_t x = 0; x < width; ++x)
        {
            const uint16_t px = c8PlatformGetPixelCoords(platform, x);
            const uint16_t py = c8PlatformGetPixelCoords(platform, y);

            const uint8_t val =
                c8PixelBufferGet(&platform->impl->pixelBuffer, x, y);

            const uint8_t newChar = val ?  pixelOn : pixelOff;

            for (size_t i = 0; i < platform->pixelSize; ++i)
            {
                for (size_t j = 0; j < platform->pixelSize; ++j)
                {
                    mvprintw(py + i, px + j, "%c", newChar);
                }
            }
        }
    }

    refresh();
}

static void _platformNcursesOpen(struct C8Platform* platform)
{
    initscr();
    cbreak();
    noecho();
    curs_set(FALSE);
    nodelay(stdscr, TRUE);
    clear();
    refresh();

    _platformNcursesSetExtendedMode(platform, 0);
}

static void _platformNcursesClose(struct C8Platform* platform)
{
    endwin();
}

static int8_t _platformNcursesGetInput(struct C8Platform* platform)
{
    int v = getch();
    int8_t input = C8_INPUT_NONE;

    if (ERR != v && platform->impl->keyRepeat)
    {
        ungetch(v);
        platform->impl->keyRepeat--;

        if (v >= '0' && v <= '9')
        {
            input = v - '0';
        }
        else if (v >= 'a' && v <= 'f')
        {
            input = (v - 'a') + 0x0a;
        }
        else if (0x1b == v)
        {
            input = C8_INPUT_QUIT;
        }
    }
    else
    {
        platform->impl->keyRepeat = 10;
    }

    return input;
}

static void _platformNcursesClearDisplay(struct C8Platform* platform)
{
    c8PixelBufferClearAll(&platform->impl->pixelBuffer);
    clear();
    refresh();
}

static uint8_t _platformNcursesDisplaySprite(
        struct C8Platform* platform,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n)
{
    const uint8_t isPixelCleared = c8PlatformCommonDisplaySprite(platform,
            &platform->impl->pixelBuffer,
            data,
            x,
            y,
            n);
    _platformNcursesBlitPixels(platform);
    return isPixelCleared;
}

static void _platformNcursesStep(struct C8Platform* platform)
{
    platform->input = _platformNcursesGetInput(platform);
}

static void _platformNcursesNoBeep(struct C8Platform* platform, uint8_t beepOn)
{
    platform->impl->beepOn = beepOn;
}

static void _platformNcursesScrollHoriz(struct C8Platform* platform,
        uint8_t direction)
{
    c8PixelBufferScrollHoriz(&platform->impl->pixelBuffer, direction);
    _platformNcursesBlitPixels(platform);
}

static void _platformNcursesScrollVert(struct C8Platform* platform,
        uint8_t n)
{
    if (n == 0)
    {
        return;
    }

    c8PixelBufferScrollVert(&platform->impl->pixelBuffer, n);
    _platformNcursesBlitPixels(platform);
}

static void _platformNcursesSetExtendedMode(struct C8Platform* platform,
        uint8_t isEnabled)
{
    c8PixelBufferSetHalfMode(&platform->impl->pixelBuffer,
            !isEnabled);
}

uint8_t c8PlatformImplInitialize(struct C8Platform* platform)
{
    platform->impl = malloc(sizeof(struct C8PlatformImpl));
    platform->platformOpen = _platformNcursesOpen;
    platform->platformClose = _platformNcursesClose;
    platform->getInput = _platformNcursesGetInput;
    platform->clearDisplay = _platformNcursesClearDisplay;
    platform->displaySprite = _platformNcursesDisplaySprite;
    platform->platformStep = _platformNcursesStep;
    platform->beepControl = _platformNcursesNoBeep;
    platform->scrollHoriz = _platformNcursesScrollHoriz;
    platform->scrollVert = _platformNcursesScrollVert;
    platform->setExtendedMode = _platformNcursesSetExtendedMode;

    c8PixelBufferInitialise(&platform->impl->pixelBuffer,
            platform->screenWidth,
            platform->screenHeight);

    return 0;
}

void c8PlatformImplDeInitialize(struct C8Platform* platform)
{
    if (platform->impl)
    {
        c8PixelBufferDeinitialise(&platform->impl->pixelBuffer);
        free(platform->impl);
    }
}

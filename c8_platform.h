#ifndef C8_PLATFORM_H_
#define C8_PLATFORM_H_

#include <stdint.h>

#define C8_SCREEN_W 64
#define C8_SCREEN_H 32
#define C48_SCREEN_W 128
#define C48_SCREEN_H 64
#define C48_SCROLL_LEFT 0x00
#define C48_SCROLL_RIGHT 0x01

#define C8_INPUT_NONE -1
#define C8_INPUT_QUIT -2

/// forward declaration of the platform type
struct C8Platform;

/// Function pointer type for initialization routine
typedef void (*C8PlatformOpenFn)(struct C8Platform* platform);

/// Function pointer type for step routine
typedef void (*C8PlatformStepFn)(struct C8Platform* platform);

/// Function pointer type for deinitialization routine
typedef void (*C8PlatformCloseFn)(struct C8Platform* platform);

/// Function pointer type for input collection routine
typedef int8_t (*C8PlatformGetInputFn)(struct C8Platform* platform);

/// Function pointer type for display clear routine
typedef void (*C8PlatformClearDisplayFn)(struct C8Platform* platform);

/// Function pointer to sprite display routine
typedef uint8_t (*C8PlatformDisplaySpriteFn)(struct C8Platform* platform,
        uint8_t* data, uint8_t x, uint8_t y, uint8_t n);

/// Function pointer to beep control routine
typedef void (*C8PlatformBeepControlFn)(struct C8Platform* platform,
        uint8_t beepOn);

/// Function pointer to horizontal scrolling routine
typedef void (*C8PlatformScrollHorizFn)(struct C8Platform* platform,
        uint8_t direction);

/// Function pointer to vertical scrolling routine
typedef void (*C8PlatformScrollVertFn)(struct C8Platform* platform,
        uint8_t nLines);

/// Function pointer to toggle extended mode
typedef void (*C8PlatformSetExtendedModeFn)(struct C8Platform* platform,
        uint8_t isEnabled);

/**
 * @brief Describes platform specific drawing/input routines
 */
struct C8Platform
{
    /// pointer to the platform specific implementation
    struct C8PlatformImpl* impl;

    /// character currently pressed
    int8_t input;

    /// screenWidth
    uint8_t screenWidth;

    /// screenHeight
    uint8_t screenHeight;

    /// size of the pixel square representing a c8 display pixel
    uint8_t pixelSize;

    /// Super Chip8 extended mode
    uint8_t extendedMode;

    /// used to initialize the platform
    C8PlatformOpenFn platformOpen;

    /// used to refresh platform state
    C8PlatformStepFn platformStep;

    /// used to de-initialize the platform
    C8PlatformCloseFn platformClose;

    /// get input routine pointer
    C8PlatformGetInputFn getInput;

    /// display clear routine pointer
    C8PlatformClearDisplayFn clearDisplay;

    /// display sprite at given coordinates
    C8PlatformDisplaySpriteFn displaySprite;

    /// beep control routine pointer
    C8PlatformBeepControlFn beepControl;

    /// scroll horizontally by 4 pixels
    C8PlatformScrollHorizFn scrollHoriz;

    /// scroll vertically by n lines down
    C8PlatformScrollVertFn scrollVert;

    /// switches the platform into extended mode
    C8PlatformSetExtendedModeFn setExtendedMode;
};

/// initializes an implementation of the platform
uint8_t c8PlatformInitialize(struct C8Platform* platform,
        uint8_t w, uint8_t h, uint8_t pixelSize);
void c8PlatformDeInitialize(struct C8Platform* platform);

uint8_t c8PlatformGetPixelSize(struct C8Platform* platform);

uint8_t c8PlatformGetScreenWidth(struct C8Platform* platform);

uint8_t c8PlatformGetScreenHeight(struct C8Platform* platform);

uint16_t c8PlatformGetPixelCoords(struct C8Platform* platform, uint8_t pos);

uint8_t c8PlatformIsXDraw(struct C8Platform* platform, uint8_t n);

#endif

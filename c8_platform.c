#include "c8_platform.h"

/// platform implementation should provide these two functions
extern uint8_t c8PlatformImplInitialize(struct C8Platform* platform);
extern void c8PlatformImplDeInitialize(struct C8Platform* platform);

uint8_t c8PlatformInitialize(struct C8Platform* platform,
        uint8_t w, uint8_t h, uint8_t pixelSize)
{
    platform->input = C8_INPUT_NONE;
    platform->screenWidth = w;
    platform->screenHeight = h;
    platform->extendedMode = 0;
    platform->pixelSize = pixelSize;
    return c8PlatformImplInitialize(platform);
}

uint8_t c8PlatformGetPixelSize(struct C8Platform* platform)
{
    return platform->pixelSize >> platform->extendedMode;
}

uint8_t c8PlatformGetScreenWidth(struct C8Platform* platform)
{
    return platform->screenWidth >> (1 - platform->extendedMode);
}

uint8_t c8PlatformGetScreenHeight(struct C8Platform* platform)
{
    return platform->screenHeight >> (1 - platform->extendedMode);
}

uint16_t c8PlatformGetPixelCoords(struct C8Platform* platform,
        uint8_t pos)
{
    return c8PlatformGetPixelSize(platform) * pos;
}

uint8_t c8PlatformIsXDraw(struct C8Platform* platform, uint8_t n)
{
    return n == 0 && platform->extendedMode;
}

void c8PlatformDeInitialize(struct C8Platform* platform)
{
    c8PlatformImplDeInitialize(platform);
}
